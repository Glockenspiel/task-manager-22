package ru.t1.sukhorukova.tm.repository;

import ru.t1.sukhorukova.tm.model.AbstractUserOwnerModel;
import ru.t1.sukhorukova.tm.api.repository.IUserOwnerRepository;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnerRepository<M extends AbstractUserOwnerModel> extends AbstractRepository<M> implements IUserOwnerRepository<M> {

    @Override
    public M add(final String userId, final M model) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public List<M> findAll(final String userId) {
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Comparator comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    public M findOneById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter((m -> id.equals(m.getId())))
                .findFirst().orElse(null);
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        if (userId == null || index == null) return null;
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .skip(index)
                .findFirst().orElse(null);
    }

    @Override
    public M removeOne(final String userId, final M model) {
        if (userId == null || model == null) return null;
        return removeOneById(userId, model.getId());
    }

    @Override
    public M removeOneById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        final M model = findOneById(userId, id);
        if (model == null) return null;
        return removeOne(model);
    }

    @Override
    public M removeOneByIndex(final String userId, final Integer index) {
        if (userId == null || index == null) return null;
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return removeOne(model);
    }

    @Override
    public void removeAll(final String userId) {
        final List<M> models = findAll(userId);
        for (final M model: models) removeOne(userId, model);
    }

    @Override
    public int getSize(final String userId) {
        return (int) records.stream()
                .filter(m->userId.equals(m.getUserId()))
                .count();
    }

    @Override
    public boolean existsById(final String userId, String id) {
        return findOneById(userId, id) != null;
    }

}
