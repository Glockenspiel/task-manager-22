package ru.t1.sukhorukova.tm.repository;

import ru.t1.sukhorukova.tm.model.AbstractModel;
import ru.t1.sukhorukova.tm.api.repository.IRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> records = new ArrayList<>();

    @Override
    public M add(final M model) {
        records.add(model);
        return model;
    }

    @Override
    public List<M> findAll() {
        return records;
    }

    @Override
    public List<M> findAll(final Comparator comparator) {
        final List<M> result = new ArrayList<>(records);
        result.sort(comparator);
        return result;
    }

    @Override
    public M findOneById(final String id) {
        return records.stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst().orElse(null);

    }

    @Override
    public M findOneByIndex(final Integer index) {
        return records.get(index);
    }

    @Override
    public M removeOne(final M model) {
        records.remove(model);
        return model;
    }

    @Override
    public M removeOneById(final String id) {
        final M model = findOneById(id);
        if (model == null) return null;
        records.remove(model);
        return model;
    }

    @Override
    public M removeOneByIndex(final Integer index) {
        final M model = findOneByIndex(index);
        if (model == null) return null;
        records.remove(model);
        return model;
    }

    @Override
    public void removeAll() {
        records.clear();
    }

    @Override
    public int getSize() {
        return records.size();
    }

    @Override
    public boolean existsById(String id) {
        return findOneById(id) != null;
    }

}
