package ru.t1.sukhorukova.tm.command.user;

import ru.t1.sukhorukova.tm.api.service.IAuthService;
import ru.t1.sukhorukova.tm.api.service.IUserService;
import ru.t1.sukhorukova.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected IUserService getUserService() {
        return getLocatorService().getUserService();
    }

    protected IAuthService getAuthService() {
        return getLocatorService().getAuthService();
    }

    @Override
    public String getArgument() {
        return null;
    }

}
