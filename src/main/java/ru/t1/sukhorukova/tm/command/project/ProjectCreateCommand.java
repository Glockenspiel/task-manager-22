package ru.t1.sukhorukova.tm.command.project;

import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    public static final String NAME = "project-create";
    public static final String DESCRIPTION = "Create new project.";

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");

        System.out.println("Enter project name:");
        final String name = TerminalUtil.nextLine();

        System.out.println("Enter project description:");
        final String description = TerminalUtil.nextLine();

        getProjectService().create(getAuthService().getUserId(), name, description);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
