package ru.t1.sukhorukova.tm.command.task;

import ru.t1.sukhorukova.tm.enumerated.TaskSort;
import ru.t1.sukhorukova.tm.model.Task;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    public static final String NAME = "task-list";
    public static final String DESCRIPTION = "Show task list.";

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("Enter sort:");
        System.out.println(Arrays.toString(TaskSort.values()));
        final String sortType = TerminalUtil.nextLine();

        final String userId = getAuthService().getUserId();
        final TaskSort sort = TaskSort.toSort(sortType);
        final Comparator<Task> comparator = sort != null ? sort.getComparator() : null;
        final List<Task> tasks = getTaskService().findAll(userId, comparator);

        renderTasks(tasks);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
