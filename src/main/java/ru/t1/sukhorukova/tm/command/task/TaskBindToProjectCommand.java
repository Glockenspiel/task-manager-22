package ru.t1.sukhorukova.tm.command.task;

import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    public static final String NAME = "task-bind-to-project";
    public static final String DESCRIPTION = "Bind task to project.";

    @Override
    public void execute() {
        System.out.println("BIND TASK TO PROJECT");

        System.out.println("Enter project id:");
        final String projectId = TerminalUtil.nextLine();

        System.out.println("Enter task id:");
        final String taskId = TerminalUtil.nextLine();

        getProjectTaskService().bindTaskToProject(getAuthService().getUserId(), projectId, taskId);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
