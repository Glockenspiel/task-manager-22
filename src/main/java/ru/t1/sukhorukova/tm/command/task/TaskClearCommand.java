package ru.t1.sukhorukova.tm.command.task;

public final class TaskClearCommand extends AbstractTaskCommand {

    public static final String NAME = "task-clear";
    public static final String DESCRIPTION = "Remove all tasks.";

    @Override
    public void execute() {
        System.out.println("[TASK CLEAR]");
        getTaskService().removeAll(getAuthService().getUserId());
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
