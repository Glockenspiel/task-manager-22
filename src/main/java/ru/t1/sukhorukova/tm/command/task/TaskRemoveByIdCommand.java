package ru.t1.sukhorukova.tm.command.task;

import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-remove-by-id";
    public static final String DESCRIPTION = "Remove task by id.";

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY ID]");

        System.out.println("Enter task id:");
        final String id = TerminalUtil.nextLine();

        getTaskService().removeOneById(getAuthService().getUserId(), id);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
