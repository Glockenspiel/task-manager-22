package ru.t1.sukhorukova.tm.command.user;

import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractUserCommand {

    private final String NAME = "user-unlock";
    private final String DESCRIPTION = "User unlock.";

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");

        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();

        getUserService().unlockOneByLogin(login);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
