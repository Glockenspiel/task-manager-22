package ru.t1.sukhorukova.tm.command.project;

import ru.t1.sukhorukova.tm.enumerated.ProjectSort;
import ru.t1.sukhorukova.tm.model.Project;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    public static final String NAME = "project-list";
    public static final String DESCRIPTION = "Show project list.";

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("Enter sort:");
        System.out.println(Arrays.toString(ProjectSort.values()));
        final String sortType = TerminalUtil.nextLine();

        final String userId = getAuthService().getUserId();
        final ProjectSort sort = ProjectSort.toSort(sortType);
        final Comparator<Project> comparator = sort != null ? sort.getComparator() : null;
        final List<Project> projects = getProjectService().findAll(userId, comparator);

        int index = 1;
        for (final Project project: projects) {
            if (project == null) continue;
            System.out.println(index++ + ". " + project.getName() + ": " + project.getDescription());
        }
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
