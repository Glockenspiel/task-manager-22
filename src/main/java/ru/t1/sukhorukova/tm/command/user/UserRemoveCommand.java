package ru.t1.sukhorukova.tm.command.user;

import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand {

    private final String NAME = "user-remove";
    private final String DESCRIPTION = "User remove.";

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");

        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();

        getUserService().removeOneByLogin(login);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
