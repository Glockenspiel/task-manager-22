package ru.t1.sukhorukova.tm.api.service;

import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.model.User;

public interface IUserService extends IService<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeOneByLogin(String login);

    User removeOneByEmail(String email);

    User setPassword(String id, String password);

    User updateUser(
            String id,
            String firstName,
            String lastName,
            String middleName
    );

    User lockOneByLogin(String login);

    User unlockOneByLogin(String login);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

}
