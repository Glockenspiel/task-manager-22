package ru.t1.sukhorukova.tm.api.repository;

import ru.t1.sukhorukova.tm.model.AbstractUserOwnerModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnerRepository<M extends AbstractUserOwnerModel> extends IRepository<M> {

    M add(String userId, M model);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator comparator);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    M removeOne(String userId, M model);

    M removeOneById(String userId, String id);

    M removeOneByIndex(String userId, Integer index);

    void removeAll(String userId);

    int getSize(String userId);

    boolean existsById(String userId, String id);

}
