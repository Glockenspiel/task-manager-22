package ru.t1.sukhorukova.tm.service;

import ru.t1.sukhorukova.tm.api.repository.IRepository;
import ru.t1.sukhorukova.tm.exception.entity.EntityNotFoundException;
import ru.t1.sukhorukova.tm.exception.field.IdEmptyException;
import ru.t1.sukhorukova.tm.exception.field.IndexIncorrectException;
import ru.t1.sukhorukova.tm.model.AbstractModel;
import ru.t1.sukhorukova.tm.api.service.IService;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService <M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(R repository) {
        this.repository = repository;
    }

    @Override
    public M add(final M model) {
        if (model == null) throw new EntityNotFoundException();
        return repository.add(model);
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public void removeAll() {
        repository.removeAll();
    }

    @Override
    public M findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        if (index == null || index < 0 || index >= repository.getSize()) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Override
    public M removeOne(final M model) {
        if (model == null) throw new EntityNotFoundException();
        return repository.removeOne(model);
    }

    @Override
    public M removeOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeOneById(id);
    }

    @Override
    public M removeOneByIndex(final Integer index) {
        if (index == null || index < 0 || index >= repository.getSize()) throw new IndexIncorrectException();
        return repository.removeOneByIndex(index);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

}
