package ru.t1.sukhorukova.tm.service;

import ru.t1.sukhorukova.tm.api.service.IUserService;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.exception.field.LoginEmptyException;
import ru.t1.sukhorukova.tm.exception.field.PasswordEmptyException;
import ru.t1.sukhorukova.tm.exception.user.AccessDeniedException;
import ru.t1.sukhorukova.tm.exception.user.IncorrectLoginOrPasswordException;
import ru.t1.sukhorukova.tm.exception.user.LockedException;
import ru.t1.sukhorukova.tm.exception.user.PermissionException;
import ru.t1.sukhorukova.tm.model.User;
import ru.t1.sukhorukova.tm.util.HashUtil;

import java.util.Arrays;

public final class AuthService implements ru.t1.sukhorukova.tm.api.service.IAuthService {

    private final IUserService userService;
    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();

        final User user = userService.findByLogin(login);
        if (user == null) throw new IncorrectLoginOrPasswordException();

        final boolean locked = user.isLocked() == null || user.isLocked();
        if (user.isLocked()) throw new LockedException();

        final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        final User user = userService.findOneById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

    @Override
    public void checkRoles(final Role[] roles) {
        if (roles == null) return;
        final User user = getUser();
        final Role role = user.getRole();
        if (role == null) throw new PermissionException();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}
